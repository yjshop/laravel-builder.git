<?php

namespace Eugenes\LaravelBuilder\Builder;

class WhereHasNotIn extends WhereHasIn
{
    /**
     * @var string
     */
    protected $method = 'whereNotIn';
}
